/* Local Variables: */
/* mode: C */
/* compile-command: "make -j 4" */
/* End: */

/* Multi line comment
   ESC j will make a new line
   ALT ; will make a comment block
   M-x comment-box make a comment box
*/

/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/***************/
/* Comment box */
/***************/

#include "ch.h"
#include "hal.h"
#include "shell.h"
#include "chprintf.h"
#include "test.h"
#include <stdlib.h>

#include "usbcfg.h"

/* Virtual serial port over USB.*/
SerialUSBDriver SDU1;

Thread *shelltp = NULL;

BinarySemaphore my_bsem;
BinarySemaphore my_bsem2;

/*===========================================================================*/
/* Command line related.                                                     */
/*===========================================================================*/

#define SHELL_WA_SIZE   THD_WA_SIZE(2048) /* shell size */
#define TEST_WA_SIZE    THD_WA_SIZE(256)

static void cmd_mem(BaseSequentialStream *chp, int argc, char *argv[]) {
  size_t n, size;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: mem\r\n");
    return;
  }
  n = chHeapStatus(NULL, &size);
  chprintf(chp, "core free memory : %u bytes\r\n", chCoreStatus());
  chprintf(chp, "heap fragments   : %u\r\n", n);
  chprintf(chp, "heap free total  : %u bytes\r\n", size);
}

static void cmd_threads(BaseSequentialStream *chp, int argc, char *argv[]) {
  static const char *states[] = {THD_STATE_NAMES};
  Thread *tp;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: threads\r\n");
    return;
  }
  chprintf(chp, "%10s %10s %10s %6s %6s %11s %7s\r\n", "name", "add", "stack", "prio", "refs", "state", "time");

  tp = chRegFirstThread();

  do {
      chprintf(chp, "%10s %.10lx %.10lx %6lu %6lu %11s %7lu\r\n",
               (uint32_t)tp->p_name, (uint32_t)tp, (uint32_t)tp->p_ctx.r13,
               (uint32_t)tp->p_prio, (uint32_t)(tp->p_refs - 1),
               states[tp->p_state], (uint32_t)tp->p_time);
    tp = chRegNextThread(tp);
  } while (tp != NULL);
}

static void cmd_test(BaseSequentialStream *chp, int argc, char *argv[]) {
  Thread *tp;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: test\r\n");
    return;
  }
  tp = chThdCreateFromHeap(NULL, TEST_WA_SIZE, chThdGetPriority(),
                           TestThread, chp);
  if (tp == NULL) {
    chprintf(chp, "out of memory\r\n");
    return;
  }
  chThdWait(tp);
}

/* cmd gpio
   it will parse gpio port
   pin number
   pin state
*/

static void cmd_gpio(BaseSequentialStream *chp, int argc, char *argv[]){
  
    (void)argv;

    /* This thread will toggle GPIO pins directly
       gpio then
       port?
       pin?
       H/L
    */
    
    /*******************************************************************/
    /* /\* Argumentum *\/					       */
    /* if (argc > 0) {						       */
    /*   chprintf(chp, "Usage: gpio PA 0 L to turn GPIOA to low\r\n"); */
    /*   return;						       */
    /* 	}							       */
    /*******************************************************************/
    
    /* Missing parameter */
    
    if (argc < 3) {
      chprintf(chp, "Missing parameter\r\n");
      chprintf(chp, "Usage: gpio a 0 l to turn GPIOA to low\r\n");
      chprintf(chp, "a can be a, b, c, d, e, pin number can be 0-15, l-low h-high\r\n");
      chprintf(chp, "Be careful! It won't check if it is a pin used for other purposes!\r\n");
      return;
    }
    
    /* Parse */
    char *s_port = argv[0];
    char *s_pin = argv[1];
    char *s_state = argv[2];
    uint8_t pin_state;
    uint8_t pin_number;

    /*********************/
    /* chprintf(chp, s); */
    /*********************/

    /*******************/
    /* parse pin_state */
    /*******************/

    switch (*s_state){
    case 'l': pin_state = 0;
	break;
    case 'h': pin_state = 1;
	break;
    }

    pin_number = atoi((const char *)s_pin);

    /**************/
    /* parse port */
    /**************/
    
    switch (*s_port){
    case 'a':
	{
	    palSetPadMode(GPIOA, pin_number, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);
            chprintf(chp, "GPIOA ");
	    chprintf(chp,"PIN ");
	    chprintf(chp, s_pin);
	    chprintf(chp, " ");
	    if (pin_state == 0 ) {
		chprintf(chp, "setting low");
		palClearPad(GPIOA, pin_number);
	    }
	    if (pin_state == 1 ) {
		chprintf(chp, "setting high");
		palSetPad(GPIOA, pin_number);
	    }
	    chprintf(chp, "\r\n");
	}	
	break;

    case 'b':
	{
	    palSetPadMode(GPIOB, pin_number, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);
	    chprintf(chp, "GPIOB ");
	    chprintf(chp,"PIN ");
	    chprintf(chp, s_pin);
	    chprintf(chp, " ");
	    if (pin_state == 0 ) {
		chprintf(chp, "setting low");
		palClearPad(GPIOB, pin_number);
	    }
	    if (pin_state == 1 ) {
		chprintf(chp, "setting high");
		palSetPad(GPIOB, pin_number);
	    }
	    chprintf(chp, "\r\n");
	break;
    }

    case 'c':
	{
	    palSetPadMode(GPIOC, pin_number, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);
	    chprintf(chp, "GPIOC ");
	    chprintf(chp,"PIN ");
	    chprintf(chp, s_pin);
	    chprintf(chp, " ");
	    if (pin_state == 0 ) {
		chprintf(chp, "setting low"); 
		palClearPad(GPIOC, pin_number);
	    }
	    if (pin_state == 1 ) {
		chprintf(chp, "setting high");
		palSetPad(GPIOC, pin_number);
	    }
	    chprintf(chp, "\r\n");
	break;

    case 'd':
	{
	    palSetPadMode(GPIOD, pin_number, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);
	    chprintf(chp, "GPIOD ");
	    chprintf(chp,"PIN ");
	    chprintf(chp, s_pin);
	    chprintf(chp, " ");
	    if (pin_state == 0 ) {
		chprintf(chp, "setting low"); 
		palClearPad(GPIOD, pin_number);
	    }
	    
	    if (pin_state == 1 ) {
		chprintf(chp, "setting high"); 
		palSetPad(GPIOD, pin_number);
	    }
	    chprintf(chp, "\r\n");
	break;
	}
	}
    }
    return;
 }

static const ShellCommand commands[] = {
  {"mem", cmd_mem},
  {"threads", cmd_threads},
  {"test", cmd_test},
  {"gpio", cmd_gpio},
  {NULL, NULL}
};

static const ShellConfig shell_cfg1 = {
  (BaseSequentialStream *)&SDU1,
  commands
};

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();


  /*
   * Initialize semaphore.
   */

   chBSemInit(&my_bsem, TRUE);
   chBSemInit(&my_bsem2, TRUE);

  /*
   * Activates the serial driver 2 using the driver default configuration.
   * PA2(TX) and PA3(RX) are routed to USART2.
   */
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));

  palSetPadMode(GPIOD, 12, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);
  palClearPad(GPIOD, 12);
  palSetPadMode(GPIOD, 14, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);
  palClearPad(GPIOD, 14);

  palSetPad(GPIOD, 14);
  /*
   * Shell manager initialization.
   */
  shellInit();

  /*
   * Initializes a serial-over-USB CDC driver.
   */
  sduObjectInit(&SDU1);
  sduStart(&SDU1, &serusbcfg);

  /*
   * Activates the USB driver and then the USB bus pull-up on D+.
   * Note, a delay is inserted in order to not have to disconnect the cable
   * after a reset.
   */
  usbDisconnectBus(serusbcfg.usbp);
  chThdSleepMilliseconds(1000);
  usbStart(serusbcfg.usbp, &usbcfg);
  usbConnectBus(serusbcfg.usbp);

  shelltp = shellCreate(&shell_cfg1, SHELL_WA_SIZE, NORMALPRIO);
  /*
   * Normal main() thread activity, in this demo it just performs
   * a shell respawn upon its termination.
   */
  while (TRUE) {
    if (!shelltp) {
      if (SDU1.config->usbp->state == USB_ACTIVE) {
        /* Spawns a new shell.*/
        shelltp = shellCreate(&shell_cfg1, SHELL_WA_SIZE, NORMALPRIO);
      }
    }
    else {
      /* If the previous shell exited.*/
      if (chThdTerminated(shelltp)) {
        /* Recovers memory of the previous shell.*/
        chThdRelease(shelltp);
        shelltp = NULL;
      }
    }
    chThdSleepMilliseconds(500);
  }
}
