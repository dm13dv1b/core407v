target remote localhost:2331
monitor speed 1000
monitor endian little
monitor flash device = STM32F407VE
#monitor flash breakpoints = 1
monitor reset
#monitor speed auto
#set remote memory-write-packet-size 1024
#set remote memory-write-packet-size fixed
monitor halt
load build/ch.elf
monitor reset
monitor halt
